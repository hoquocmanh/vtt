import { lazy } from "react";
// project imports
import MainLayout from "layout/MainLayout";
import Loadable from "ui-component/Loadable";
import AuthGuard from "utils/route-guard/AuthGuard";

const TestTheirCharacteristics = Loadable(
  lazy(() => import("views/dashboard-admin/TestTheirCharacteristics"))
);
const ResultCheck = Loadable(
  lazy(() => import("views/dashboard-admin/resultCheck"))
);

const MainRoutes = {
  path: "/",
  element: (
    <AuthGuard>
      <MainLayout />
    </AuthGuard>
  ),
  children: [
    {
      path: "/tracnghiemtinhcach",
      element: <TestTheirCharacteristics />,
    },
    {
      path: "/ketquatracnghiem",
      element: <ResultCheck />,
    },
  ]
};

export default MainRoutes;
