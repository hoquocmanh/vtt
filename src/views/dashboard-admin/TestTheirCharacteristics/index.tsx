// material-ui
import { useEffect, useState } from 'react';
import { DataQuestion } from './dataQuestion';
import PointTable from './point';
import StickyHeadTable from './table';
import axios from 'axios';
import { CircularProgress } from '@mui/material';

//import components

export default function ManageUser() {


    const userName = localStorage.getItem('nameUser')
    const userEmail = localStorage.getItem('emailUser')
    const userPhone = localStorage.getItem('phoneUser')

    const [isStatus, setStatus] = useState(false);
    const [isLoading, setLoading] = useState(true);


    useEffect(() => {
        axios.get('https://vtt-be-v2.onrender.com/api/checking')
            .then(function (response) {
                setLoading(false)
            })
            .catch(function (error) {
                console.log(error);
                setLoading(true)
            });
    }, [])

    useEffect(() => {
        if (userName !== null || userEmail !== null || userPhone !== null) {
            setStatus(true)
        }
    }, [userName, userEmail, userPhone])

    // localStorage.removeItem('nameUser')
    // localStorage.removeItem('emailUser')
    // localStorage.removeItem('phoneUser')

    return (
        <div>
            {/* {
                !true ? <StickyHeadTable projectItem={dataUser}/> : <CircularProgress />
            } */}
            {!isStatus ?
                <>
                    {
                        !isLoading ? <PointTable statusSubmit={(e: any) => setStatus(e)} /> : <CircularProgress />
                    }
                </>
                :
                <>
                    {
                        !isLoading ? <StickyHeadTable projectItem={DataQuestion} /> : <CircularProgress />
                    }
                </>
            }
        </div>
    );
}
