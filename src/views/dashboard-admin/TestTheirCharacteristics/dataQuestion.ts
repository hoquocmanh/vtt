export const DataQuestion = [
    {
        idQuestion: 1,
        question: 'Bạn thích công việc kiểu:',
        answer: {
            A: 'Xã hội, Giao tiếp.',
            B: 'Thiết kế, nghiên cứu.'
        }
    },
    {
        idQuestion: 2,
        question: 'Bạn tiếp xúc với một người theo xu hướng:',
        answer: {
            A: 'Khách quan.',
            B: 'Chủ quan.'
        }
    },
    {
        idQuestion: 3,
        question: 'Bạn đánh giá sự việc dựa trên:',
        answer: {
            A: 'Ý nghĩa, giá trị.',
            B: 'Tính logic.'
        }
    },
    {
        idQuestion: 4,
        question: 'Bạn là người có tác phong:',
        answer: {
            A: 'Thong thả, thoải mái.',
            B: 'Nhanh nhẹn, đúng giờ.'
        }
    },
    {
        idQuestion: 5,
        question: 'Trong các cuộc giao tiếp, bạn là người:',
        answer: {
            A: 'Bắt đầu cho cuộc nói chuyện.',
            B: 'Chỉ nói khi người khác bắt chuyện.'
        }
    },
    {
        idQuestion: 6,
        question: 'Bạn là người thế nào:',
        answer: {
            A: 'Là người có cái đầu lạnh.',
            B: 'Là người có trái tim ấm.'
        }
    },
    {
        idQuestion: 7,
        question: 'Tính cách của bạn:',
        answer: {
            A: 'Dễ chịu, thoải mái.',
            B: 'Nghiêm chỉnh, quả quyết.'
        }
    },
    {
        idQuestion: 8,
        question: 'Những người có tầm nhìn xa thường:',
        answer: {
            A: 'Khá thú vị.',
            B: 'Khó hiểu.'
        }
    },
    {
        idQuestion: 9,
        question: 'Đối với những người mới gặp, bạn thường:',
        answer: {
            A: 'Dễ dàng bắt chuyện và trò chuyện nhiều điều cùng họ.',
            B: 'Cảm thấy khó khăn và không biết nên nói gì.'
        }
    },
    {
        idQuestion: 10,
        question: 'Một công việc gây nhàm chán khi:',
        answer: {
            A: 'Không có quy trình cụ thể.',
            B: 'Thiếu tính sáng tạo.'
        }
    },
    {
        idQuestion: 11,
        question: 'Bạn bị hấp dẫn bởi:',
        answer: {
            A: 'Sự hòa hợp với mọi người.',
            B: 'Sự nhất quán trong suy nghĩ.'
        }
    },
    {
        idQuestion: 12,
        question: 'Bạn bị lôi cuốn bởi kiểu người:',
        answer: {
            A: 'Giàu trí tưởng tượng.',
            B: 'Nắm bắt tình huống tốt.'
        }
    },
    {
        idQuestion: 13,
        question: 'Trong các cuộc trò chuyện bạn thường:',
        answer: {
            A: 'Dè dặt.',
            B: 'Thoải mái.'
        }
    },
    {
        idQuestion: 14,
        question: 'Lựa chọn của bạn có xu hướng:',
        answer: {
            A: 'Tuân theo chuẩn mực.',
            B: 'Đôi lúc không hiểu rõ nguyên nhân.'
        }
    },
    {
        idQuestion: 15,
        question: 'Trong hai điều sau điều nào tồi tệ hơn:',
        answer: {
            A: 'Không công bằng.',
            B: 'Tàn nhẫn.'
        }
    },
    {
        idQuestion: 16,
        question: 'Tính cách nào giống bạn hơn:',
        answer: {
            A: 'Kiên quyết.',
            B: 'Nhẹ nhàng.'
        }
    },
    {
        idQuestion: 17,
        question: 'Đối với một vấn đề trong nhóm:',
        answer: {
            A: 'Độc lập, giải quyết.',
            B: 'Trao đổi với mọi người.'
        }
    },
    {
        idQuestion: 18,
        question: 'Trong cuộc sống, bạn cảm thấy hứng thú với:',
        answer: {
            A: 'Những điều bất ngờ không tính trước.',
            B: 'Những điều có kế hoạch trước.'
        }
    },
    {
        idQuestion: 19,
        question: 'Bạn cảm thấy tâm đắc với bản thân bởi sự:',
        answer: {
            A: 'Chắc chắn trong tư tưởng.',
            B: 'Cống hiến hết mình.'
        }
    },
    {
        idQuestion: 20,
        question: 'Bạn nhìn nhận một vấn đề với:',
        answer: {
            A: 'Khả năng lý luận phân tích.',
            B: 'Sự liên tưởng.'
        }
    },
    {
        idQuestion: 21,
        question: 'Trong các kỳ nghỉ lễ bạn có xu hướng:',
        answer: {
            A: 'Ở nhà cùng gia đình.',
            B: 'Đi du lịch cùng bạn bè.'
        }
    },
    {
        idQuestion: 22,
        question: 'Bạn bị lôi cuốn bởi:',
        answer: {
            A: 'Các ngụ ý, hàm ý, ẩn ý.',
            B: 'Các nguyên tắc, nguyên lý.'
        }
    },
    {
        idQuestion: 23,
        question: 'Khi quyết định việc gì bạn thường dựa vào:',
        answer: {
            A: 'Cảm nhận từ con tim.',
            B: 'Những quy tắc chung.'
        }
    },
    {
        idQuestion: 24,
        question: 'Bạn thích làm việc với người sếp:',
        answer: {
            A: 'Làm việc có phương pháp, tổ chức tốt.',
            B: 'Ứng biến xoay trở tốt trước khó khăn.'
        }
    },
    {
        idQuestion: 25,
        question: 'Bạn nghiêng về:',
        answer: {
            A: 'Sự chắc chắn.',
            B: 'Sự cởi mở.'
        }
    },
    {
        idQuestion: 26,
        question: 'Bạn là người có tư tưởng:',
        answer: {
            A: 'Công tư phân minh.',
            B: 'Bị tình cảm chi phối.'
        }
    },
    {
        idQuestion: 27,
        question: 'Bạn thích học các môn:',
        answer: {
            A: 'Nghệ thuật.',
            B: 'Khoa học.'
        }
    },
    {
        idQuestion: 28,
        question: 'Trong một nhóm bạn bè bạn thường:',
        answer: {
            A: 'Trầm lặng, ít nói.',
            B: 'Hoạt bát, vui vẻ.'
        }
    },
    {
        idQuestion: 29,
        question: 'Khi đánh giá một người bạn thường:',
        answer: {
            A: 'Dựa vào vẻ bề ngoài của họ.',
            B: 'Dựa vào cảm nhận của bạn.'
        }
    },
    {
        idQuestion: 30,
        question: 'Sự thật tai hại khi bạn:',
        answer: {
            A: 'Quá nồng nhiệt, mong chờ.',
            B: 'Quá khách quan.'
        }
    },
    {
        idQuestion: 31,
        question: 'Phong cách của bạn:',
        answer: {
            A: 'Thích những thứ quen thuộc.',
            B: 'Thích những điều mới mẻ.'
        }
    },
    {
        idQuestion: 32,
        question: 'Khi giải quyết một công việc thông thường, bạn sẽ:',
        answer: {
            A: 'Tìm kiếm một cách làm mới.',
            B: 'Làm theo cách mọi người hay làm.'
        }
    },
    {
        idQuestion: 33,
        question: 'Bạn thích làm việc:',
        answer: {
            A: 'Độc lập.',
            B: 'Theo nhóm.'
        }
    },
    {
        idQuestion: 34,
        question: 'Bạn có hành động theo bản năng:',
        answer: {
            A: 'Hiếm khi.',
            B: 'Hay xảy ra.'
        }
    },
    {
        idQuestion: 35,
        question: 'Trong cuộc sống, một người sẽ tốt hơn nếu:',
        answer: {
            A: 'Giàu lý trí.',
            B: 'Giàu cảm xúc.'
        }
    },
    {
        idQuestion: 36,
        question: 'Khi nghe một album nhạc mới, bạn thường nghe:',
        answer: {
            A: 'Theo thứ tự.',
            B: 'Phát ngẫu nhiên.'
        }
    },
    {
        idQuestion: 37,
        question: 'Bạn là người:',
        answer: {
            A: 'Khéo léo, lanh lợi.',
            B: 'Thẳng thắn, thực tế.'
        }
    },
    {
        idQuestion: 38,
        question: 'Sẽ khó khăn hơn khi bạn cố:',
        answer: {
            A: 'Sử dụng người khác.',
            B: 'Hiểu và chia sẻ với người khác.'
        }
    },
    {
        idQuestion: 39,
        question: 'Khi trình bày, bạn thường:',
        answer: {
            A: 'Diễn đạt trực tiếp nhất có thể.',
            B: 'Diễn đạt một cách gián tiếp.'
        }
    },
    {
        idQuestion: 40,
        question: 'Khi cảm thấy buồn bạn thường:',
        answer: {
            A: 'Tìm bạn bè tâm sựu hoặc đi chơi.',
            B: 'Muốn ở một mình cho khuây khỏa.'
        }
    },
    {
        idQuestion: 41,
        question: 'Đối với các hoạt động của cơ quan, lớp học:',
        answer: {
            A: 'Tham gia khi bị ép.',
            B: 'Năng nổ, tích cực.'
        }
    },
    {
        idQuestion: 42,
        question: 'Bạn cảm thấy thoải mái với:',
        answer: {
            A: 'Những thứ không xác định.',
            B: 'Những thứ logic.'
        }
    },
    {
        idQuestion: 43,
        question: 'Cảm xúc của bạn thường được người khác:',
        answer: {
            A: 'Dễ dàng nắm bắt.',
            B: 'Giấu kín, khó nhận biết.'
        }
    },
    {
        idQuestion: 44,
        question: 'Thật tồi tệ khi:',
        answer: {
            A: 'Hành xử thiếu cân nhắc.',
            B: 'Chỉ trích, phê phán người khác.'
        }
    },
    {
        idQuestion: 45,
        question: 'Bạn muốn một sự kiện như thế nào:',
        answer: {
            A: 'Chuẩn bị chu toàn trước.',
            B: 'Diễn ra tự nhiên.'
        }
    },
    {
        idQuestion: 46,
        question: 'Bạn là người có tính cách:',
        answer: {
            A: 'Dễ gần, hòa đồng.',
            B: 'Thân thiện, nhưng kín đáo.'
        }
    },
    {
        idQuestion: 47,
        question: 'Bạn có ưu điểm về:',
        answer: {
            A: 'Trí tưởng tượng phong phú.',
            B: 'Sự quan sát thực tế.'
        }
    },
    {
        idQuestion: 48,
        question: 'Khi quyết định điều gì đó, bạn tin vào:',
        answer: {
            A: 'Linh cảm',
            B: 'Sự việc thực tế.'
        }
    },
    {
        idQuestion: 49,
        question: 'Bạn muốn được người khác khen ngợi như thế nào:',
        answer: {
            A: 'Bạn là người có đầu óc suy luận.',
            B: 'Bạn là người giàu tình cảm và sự tinh tế.'
        }
    },
    {
        idQuestion: 50,
        question: 'Bạn thích các sự việc xảy ra:',
        answer: {
            A: 'Có chọn lựa và suy tính.',
            B: 'Một cách tự nhiên.'
        }
    },
    {
        idQuestion: 51,
        question: 'Bạn hay hành động một cách:',
        answer: {
            A: 'Bộc phát, linh hoạt.',
            B: 'Kỹ lưỡng, cẩn thận.'
        }
    },
    {
        idQuestion: 52,
        question: 'Bạn muốn bổ sung thêm điều gì ở bản thân:',
        answer: {
            A: 'Tình cảm dồi dào.',
            B: 'Lý trí mạnh mẽ.'
        }
    },
    {
        idQuestion: 53,
        question: 'Bạn là người:',
        answer: {
            A: 'Mơ mộng và tưởng tượng.',
            B: 'Thực tế và thực dụng.'
        }
    },
    {
        idQuestion: 54,
        question: 'Khi điện thoại bàn trông văn phòng công ty đổ chuông:',
        answer: {
            A: 'Nhấc máy trước để nghe.',
            B: 'Để cho đồng nghiệp nhấc máy.'
        }
    },
    {
        idQuestion: 55,
        question: 'Bạn mới tham gia một lớp ngoại ngữ:',
        answer: {
            A: 'Cố làm quen với nhiều người.',
            B: 'Chỉ nói chuyện với vài người.'
        }
    },
    {
        idQuestion: 56,
        question: 'Theo bạn, các sự việc, sự kiện:',
        answer: {
            A: 'Bản thân nói giải thích cho chính nó.',
            B: 'Là bằng chứng giải thích cho các quy tắc, quy luật.'
        }
    },
    {
        idQuestion: 57,
        question: 'Bạn là người:',
        answer: {
            A: 'Đa sầu đa cảm.',
            B: 'Thực dụng, không bị chi phối bởi tình cảm.'
        }
    },
    {
        idQuestion: 58,
        question: 'Bạn thường quan tâm tới người khác thông qua:',
        answer: {
            A: 'Những suy nghĩ của họ.',
            B: 'Những gì họ làm được.'
        }
    },
    {
        idQuestion: 59,
        question: 'Bạn nghiêng về điều nào sau đây:',
        answer: {
            A: 'Thói quen.',
            B: 'Những điều mới mẻ.'
        }
    },
    {
        idQuestion: 60,
        question: 'Mọi việc sẽ tốt hơn nếu:',
        answer: {
            A: 'Luôn được chuẩn bị kỹ lưỡng.',
            B: 'Tùy cơ ứng biến.'
        }
    },
    {
        idQuestion: 61,
        question: 'Bạn thích mối quan hệ:',
        answer: {
            A: 'Ít bạn nhưng chơi rất thân.',
            B: 'Nhiều bạn nhưng không thân thiết lắm.'
        }
    },
    {
        idQuestion: 62,
        question: 'Nếu bạn là nhà văn, bạn sẽ viết bài theo cách:',
        answer: {
            A: 'Diễn đạt lời văn qua các phép so sánh, ẩn dụ.',
            B: 'Nghĩ sao viết vậy, diễn đạt rõ ràng.'
        }
    },
    {
        idQuestion: 63,
        question: 'Khi quyết định một việc:',
        answer: {
            A: 'Phụ thuộc vào tâm trạng.',
            B: 'Suy tính cẩn trọng.'
        }
    },
    {
        idQuestion: 64,
        question: 'Bạn thường hay đưa ra quyết định:',
        answer: {
            A: 'Một cách chắc chắn.',
            B: 'Không chắc lắm, có thể thay đổi sau.'
        }
    },
    {
        idQuestion: 65,
        question: 'Bạn là người sống thiên về:',
        answer: {
            A: 'Nguyên tắc, lý trí.',
            B: 'Cảm xúc, tình cảm.'
        }
    },
    {
        idQuestion: 66,
        question: 'Bạn thích thú với sự việc:',
        answer: {
            A: 'Thực tế.',
            B: 'Bất ngờ, mới mẻ.'
        }
    },
    {
        idQuestion: 67,
        question: 'Khi nói về một chủ đề mới lạ chưa gặp, bạn cảm thấy:',
        answer: {
            A: 'Không hứng thú.',
            B: 'Hào hứng.'
        }
    },
    {
        idQuestion: 68,
        question: 'Đối với một suy luận được nhiều người tin dùng:',
        answer: {
            A: 'Kiểm tra lại trước khi áp dụng.',
            B: 'Tin tưởng và áp dụng theo.'
        }
    },
    {
        idQuestion: 69,
        question: 'Điều gì làm cho bạn ấn tượng:',
        answer: {
            A: 'Những thứ có tính logic cao.',
            B: 'Những thứ gây cảm động.'
        }
    },
    {
        idQuestion: 70,
        question: 'Bạn là người có xu hướng thiên về:',
        answer: {
            A: 'Những thứ thực tế và rõ ràng.',
            B: 'Những suy đoán.'
        }
    },
    {
        idQuestion: 71,
        question: 'Ở một nơi đông người làm bạn:',
        answer: {
            A: 'Không thoải mái.',
            B: 'Thích thú.'
        }
    },
    {
        idQuestion: 72,
        question: 'Bạn đánh giá một người dựa trên:',
        answer: {
            A: 'Hoàn cảnh nhất định.',
            B: 'Nguyên tắc chung.'
        }
    },
    {
        idQuestion: 73,
        question: 'Bạn được mời tới bữa tiệc của công ty:',
        answer: {
            A: 'Ra về sớm vì không hào hứng lắm.',
            B: 'Vui chơi hết mình cho đến khi tàn tiệc.'
        }
    },
    {
        idQuestion: 74,
        question: 'Khi làm việc bạn sẽ:',
        answer: {
            A: 'Tuân thủ thời gian làm việc.',
            B: 'Làm việc tùy hứng.'
        }
    },
    {
        idQuestion: 75,
        question: 'Khi đi tham dự một buổi liên hoan/tiệc tùng, bạn sẽ:',
        answer: {
            A: 'Trò chuyện cùng nhiều người, quen hoặc không quen.',
            B: 'Chỉ trò chuyện với một số người quen.'
        }
    },
    {
        idQuestion: 76,
        question: 'Điều gì làm bạn cảm thấy tồi tệ:',
        answer: {
            A: 'Những thứ mơ hồ, không rõ ràng.',
            B: 'Những thứ nhàm chán, tẻ nhạt,'
        }
    }
]