// material-ui
import { useEffect, useState } from 'react';
import StickyHeadTable from './table';
import axios from 'axios';
import { Button, CircularProgress, Grid, TextField } from '@mui/material';
import MainCardV2 from 'ui-component/cards/MainCardV2';
import styled from 'styled-components';

//import components

export default function ResultCheck() {

    const [dataQuestion, setDataQuestion] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [isValidate, setPassvalidate] = useState(false);
    const [pass, setPass] = useState('');
    const [textError, setError] = useState('');

    useEffect(() => {
        axios.get('https://vtt-be-v2.onrender.com/api/checking')
            .then(function (response) {
                setDataQuestion(response.data.checking.reverse())
                setLoading(false)
            })
            .catch(function (error) {
                console.log(error);
                setLoading(true)
            });
    }, [])

    function checkValidate() {
        if (pass === '123456') {
            setPassvalidate(true)
            setError('')
        } else {
            setPassvalidate(false)
            setError('-- Sai mã')
        }
    }

    return (
        <div>
            {isValidate ?
                <>
                    {
                        !isLoading ? <StickyHeadTable projectItem={dataQuestion} /> : <CircularProgress />
                    }
                </>
                :
                <MainCardV2 title={`Nhập mã bảo mật ${textError}`}>
                    <Grid
                        container
                        sx={{ flexWrap: "nowrap", justifyContent: "space-between" }}
                    >
                        <CssTextField onChange={(e) => setPass(e.target.value)} sx={{ width: '30%' }} id="outlined-basic" label="Mật mã" variant="outlined" />
                        <Button onClick={checkValidate} type='submit' size='large' variant="outlined" sx={{ marginRight: '10px', width: '150px', height: '50px', gap: '10px', borderColor: '#680102', color: '#680102' }}>Tiếp tục</Button>
                    </Grid>
                </MainCardV2>
            }
        </div>
    );
}

const CssTextField = styled(TextField)({
    '& label.Mui-focused': {
        color: '#680102',
    },
    '& .MuiInput-underline:after': {
        borderBottomColor: '#680102',
    },
    '& .MuiOutlinedInput-root': {

        '&:hover fieldset': {
            borderColor: '#680102',
        },
        '&.Mui-focused fieldset': {
            borderColor: '#680102',
        },
        '& input': { color: '#680102' }
    },
});