import {
  Button,
  Checkbox,
  CircularProgress,
  FormControlLabel,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow
} from "@mui/material";
import { IconSort09, IconSort90, IconSortAZ, IconSortZA } from "@tabler/icons";
import axios from "axios";
import Moment from 'moment';
import { useEffect, useState } from "react";
import { gridSpacing } from "store/constant";
import { KeyedObject } from "types";
import IBreadcrumsCustom from "ui-component/breadcrums";
import MainCardV2 from "ui-component/cards/MainCardV2";
import MainCardV3 from "ui-component/cards/MainCardV3";
import ENFJ, { ENFP, ENTJ, ENTP, ESFJ, ESFP, ESTJ, ESTP, INFJ, INFP, INTP, ISFJ, ISFP, ISTJ, ISTP } from "./GrCharacter";
import { DataQuestion } from './dataQuestion';

interface Provider {
  resultCheck: string;
  name: string,
  email: string,
  phone: string,
  arrayAnswer: [],
  timeQuestion: string
}

// ==============================|| TABLE - STICKY HEADER ||============================== //

export default function StickyHeadTable(props: {
  [x: string]: any;
  projectItem: any;

}) {

  let checkArray: any = []
  let anwerArrayB: any = []

  const [dataQuestionDetail, setDataQuestionDetails] = useState<Provider>(checkArray);
  const [listAnwerArray, setListAnwerArrayB] = useState<any[]>(anwerArrayB);
  const [isLoading, setLoading] = useState(true);
  const [isActive, setActive] = useState(false);
  const [idDetail, setIdDetail] = useState('64368718e96236974e94a51e');
  const [find, setFind] = useState('');
  const [arrayData, setArrayData] = useState(props.projectItem);

  useEffect(() => {
    const filteredRows = props.projectItem.filter((item: any) => item.name.toLowerCase().includes(find.toLowerCase()) || item.email.toLowerCase().includes(find.toLowerCase()) || item.phone.toLowerCase().includes(find.toLowerCase()) || item.resultCheck.toLowerCase().includes(find.toLowerCase()) || item.timeAnswer.toLowerCase().includes(find.toLowerCase()));
    if (find !== '') {
      setArrayData(filteredRows)
    } else {
      setArrayData(props.projectItem)
    }
  }, [find])

  const [isSortName, setSortName] = useState<any>(null);
  const [isSortEmail, setSortEmail] = useState<any>(null);
  const [isSortPhone, setSortPhone] = useState<any>(null);
  const [isSortTime, setSortTime] = useState<any>(null);
  const [isSortResult, setSortResult] = useState<any>(null);

  useEffect(() => {
    if (isSortName) {
      const sortRowsName = props.projectItem.sort(function (a: any, b: any) {
        let x = a.name.toLowerCase();
        let y = b.name.toLowerCase();
        if (x < y) { return -1; }
        if (x > y) { return 1; }
        return 0;
      });
      setArrayData(sortRowsName)
      console.log('ok');
    }

    if (!isSortName) {
      const sortRowsName = props.projectItem.sort(function (a: any, b: any) {
        let x = a.name.toLowerCase();
        let y = b.name.toLowerCase();
        if (x < y) { return 1; }
        if (x > y) { return -1; }
        return 0;
      });
      setArrayData(sortRowsName)
    }
  }, [isSortName])

  useEffect(() => {
    if (isSortEmail) {
      const sortRowsEmail = props.projectItem.sort(function (a: any, b: any) {
        let x = a.email.toLowerCase();
        let y = b.email.toLowerCase();
        if (x < y) { return -1; }
        if (x > y) { return 1; }
        return 0;
      });
      setArrayData(sortRowsEmail)
    }
    if (!isSortEmail) {
      const sortRowsEmail = props.projectItem.sort(function (a: any, b: any) {
        let x = a.email.toLowerCase();
        let y = b.email.toLowerCase();
        if (x < y) { return 1; }
        if (x > y) { return -1; }
        return 0;
      });
      setArrayData(sortRowsEmail)
    }
  }, [isSortEmail])

  useEffect(() => {
    if (isSortPhone) {
      const sortRowsPhone = props.projectItem.sort(function (a: any, b: any) {
        let x = a.phone.toLowerCase();
        let y = b.phone.toLowerCase();
        if (x < y) { return -1; }
        if (x > y) { return 1; }
        return 0;
      });
      setArrayData(sortRowsPhone)

    }
    if (!isSortPhone) {
      const sortRowsPhone = props.projectItem.sort(function (a: any, b: any) {
        let x = a.phone.toLowerCase();
        let y = b.phone.toLowerCase();
        if (x < y) { return 1; }
        if (x > y) { return -1; }
        return 0;
      });
      setArrayData(sortRowsPhone)
    }
  }, [isSortPhone])

  useEffect(() => {
    if (isSortTime) {
      const sortRowsTime = props.projectItem.sort(function (a: any, b: any) {
        let x = a.timeAnswer.toLowerCase();
        let y = b.timeAnswer.toLowerCase();
        if (x < y) { return -1; }
        if (x > y) { return 1; }
        return 0;
      });
      setArrayData(sortRowsTime)

    }
    if (!isSortTime) {
      const sortRowsTime = props.projectItem.sort(function (a: any, b: any) {
        let x = a.timeAnswer.toLowerCase();
        let y = b.timeAnswer.toLowerCase();
        if (x < y) { return 1; }
        if (x > y) { return -1; }
        return 0;
      });
      setArrayData(sortRowsTime)
    }
  }, [isSortTime])

  useEffect(() => {
    if (isSortResult) {
      const sortRowsResult = props.projectItem.sort(function (a: any, b: any) {
        let x = a.resultCheck.toLowerCase();
        let y = b.resultCheck.toLowerCase();
        if (x < y) { return -1; }
        if (x > y) { return 1; }
        return 0;
      });
      setArrayData(sortRowsResult)
    }
    if (!isSortResult) {
      const sortRowsResult = props.projectItem.sort(function (a: any, b: any) {
        let x = a.resultCheck.toLowerCase();
        let y = b.resultCheck.toLowerCase();
        if (x < y) { return 1; }
        if (x > y) { return -1; }
        return 0;
      });
      setArrayData(sortRowsResult)
    }
  }, [isSortResult])

  const col1 = [0, 7, 14, 21, 28, 35, 42, 49, 56, 63];
  const col2 = [1, 8, 15, 22, 29, 36, 43, 50, 57, 64];
  const col3 = [2, 9, 16, 23, 30, 37, 44, 51, 58, 65];
  const col4 = [3, 10, 17, 24, 31, 38, 45, 52, 59, 66];
  const col5 = [4, 11, 18, 25, 32, 39, 46, 53, 60, 67];
  const col6 = [5, 12, 19, 26, 33, 40, 47, 54, 61, 68];
  const col7 = [6, 13, 20, 27, 34, 41, 48, 55, 62, 69];

  let pointAnswerCol1: any = [];
  let pointAnswerCol2: any = [];
  let pointAnswerCol3: any = [];
  let pointAnswerCol4: any = [];
  let pointAnswerCol5: any = [];
  let pointAnswerCol6: any = [];
  let pointAnswerCol7: any = [];
  let countsPoint1: any = {};
  let countsPoint2: any = {};
  let countsPoint3: any = {};
  let countsPoint4: any = {};
  let countsPoint5: any = {};
  let countsPoint6: any = {};
  let countsPoint7: any = {};

  useEffect(() => {
    const newPoin = listAnwerArray

    axios.get(`https://vtt-be-v2.onrender.com/api/checking/${idDetail}`)
      .then(function (response) {
        setDataQuestionDetails(response.data.checking)
        setListAnwerArrayB(response.data.checking)
        setLoading(false)

        for (let i = 0; i < 76; i++) {
          if (response.data.checking?.arrayAnswer[i] === DataQuestion[i].answer.A) {
            newPoin[i] = 'A'
          }
          if (response.data.checking?.arrayAnswer[i] === DataQuestion[i].answer.B) {
            newPoin[i] = 'B'
          }
        }
        setListAnwerArrayB(newPoin)

        for (let x = 0; x < col1.length; x++) {
          pointAnswerCol1[x] = listAnwerArray[col1[x]]
          pointAnswerCol2[x] = listAnwerArray[col2[x]]
          pointAnswerCol3[x] = listAnwerArray[col3[x]]
          pointAnswerCol4[x] = listAnwerArray[col4[x]]
          pointAnswerCol5[x] = listAnwerArray[col5[x]]
          pointAnswerCol6[x] = listAnwerArray[col6[x]]
          pointAnswerCol7[x] = listAnwerArray[col7[x]]

        }

        for (const num of pointAnswerCol1) {
          countsPoint1[num] = countsPoint1[num] ? countsPoint1[num] + 1 : 1;
        }
        for (const num of pointAnswerCol2) {
          countsPoint2[num] = countsPoint2[num] ? countsPoint2[num] + 1 : 1;
        }
        for (const num of pointAnswerCol3) {
          countsPoint3[num] = countsPoint3[num] ? countsPoint3[num] + 1 : 1;
        }
        for (const num of pointAnswerCol4) {
          countsPoint4[num] = countsPoint4[num] ? countsPoint4[num] + 1 : 1;
        }
        for (const num of pointAnswerCol5) {
          countsPoint5[num] = countsPoint5[num] ? countsPoint5[num] + 1 : 1;
        }
        for (const num of pointAnswerCol6) {
          countsPoint6[num] = countsPoint6[num] ? countsPoint6[num] + 1 : 1;
        }
        for (const num of pointAnswerCol7) {
          countsPoint7[num] = countsPoint7[num] ? countsPoint7[num] + 1 : 1;
        }

      })
      .catch(function (error) {
        console.log(error);
        setLoading(true)
        setActive(false)
      });


  }, [idDetail])


  const convertDate = (dateInput: Date) => {
    return Moment(new Date(dateInput)).format('DD-MM-YYYY h:mm:ss A');
  }


  const handleDetail = (idDetail: string) => {
    setIdDetail(idDetail)
    setActive(true)
  };

  const handleBack = () => {
    setActive(false)
  };

  return (
    <>
      <IBreadcrumsCustom profile="Trắc nghiệm tính cách" mainProfile="Kết quả trắc nghiệm" link="/tracnghiemtinhcach" />
      {!isActive &&
        <MainCardV3 title={`Danh sách kết quả `} textFind={(e: any) => setFind(e)} dataExport={props.projectItem}>
          <Grid
            container
            spacing={gridSpacing}
            sx={{ flexWrap: "nowrap", justifyContent: "space-between" }}
          >
          </Grid>
          <TableContainer sx={{ maxHeight: 440 }}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <TableCell>STT</TableCell>
                  <TableCell align="left">Họ và tên {isSortName ? <IconSortZA style={{ top: '16px', position: 'absolute', color:'#680102' }} onClick={() => setSortName(false)} /> : <IconSortAZ style={{ top: '16px', position: 'absolute', color:'#680102' }} onClick={() => setSortName(true)} />} </TableCell>
                  <TableCell align="left">Email {isSortEmail ? <IconSortZA style={{ top: '16px', position: 'absolute', color:'#680102' }} onClick={() => setSortEmail(false)} /> : <IconSortAZ style={{ top: '16px', position: 'absolute', color:'#680102' }} onClick={() => setSortEmail(true)} />}</TableCell>
                  <TableCell align="left">Số điện thoại {isSortPhone ? <IconSort90 style={{ top: '16px', position: 'absolute', color:'#680102' }} onClick={() => setSortPhone(false)} /> : <IconSort09 style={{ top: '16px', position: 'absolute', color:'#680102' }} onClick={() => setSortPhone(true)} />} </TableCell>
                  <TableCell align="left">Thời gian{isSortTime ? <IconSort90 style={{ top: '16px', position: 'absolute', color:'#680102' }} onClick={() => setSortTime(false)} /> : <IconSort09 style={{ top: '16px', position: 'absolute', color:'#680102' }} onClick={() => setSortTime(true)} />} </TableCell>
                  <TableCell align="left">Kết quả{isSortResult ? <IconSortZA style={{ top: '16px', position: 'absolute', color:'#680102' }} onClick={() => setSortResult(false)} /> : <IconSortAZ style={{ top: '16px', position: 'absolute', color:'#680102' }} onClick={() => setSortResult(true)} />} </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {arrayData
                  .map((row: KeyedObject, index: number) => (
                    <TableRow
                      sx={{ py: 3 }}
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.code}
                      onClick={() => handleDetail(row._id)}
                    >
                      <TableCell component="th" scope="row">
                        {index + 1}
                      </TableCell>
                      <TableCell align="left">{row.name}</TableCell>
                      <TableCell align="left">{row.email}</TableCell>
                      <TableCell align="left">{row.phone}</TableCell>
                      <TableCell align="left">{convertDate(row.timeAnswer)}</TableCell>
                      <TableCell align="left">{row.resultCheck}</TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
        </MainCardV3>
      }
      <>
        {isActive &&
          <>
            {
              !isLoading ?
                <>
                  <MainCardV2 title={`NHÓM TÍNH CÁCH -- ${dataQuestionDetail?.resultCheck} -- NGÀNH NGHỀ PHÙ HỢP -- ${dataQuestionDetail?.name}`}>
                    <Grid
                      container
                      spacing={gridSpacing}
                      sx={{ flexWrap: "nowrap", justifyContent: "left", paddingLeft: '30px' }}
                    >
                      {dataQuestionDetail?.resultCheck === 'ENFJ' && <ENFJ />}
                      {dataQuestionDetail?.resultCheck === 'ENFP' && <ENFP />}
                      {dataQuestionDetail?.resultCheck === 'ENTJ' && <ENTJ />}
                      {dataQuestionDetail?.resultCheck === 'ENTP' && <ENTP />}
                      {dataQuestionDetail?.resultCheck === 'ESFJ' && <ESFJ />}
                      {dataQuestionDetail?.resultCheck === 'ESFP' && <ESFP />}
                      {dataQuestionDetail?.resultCheck === 'ESTJ' && <ESTJ />}
                      {dataQuestionDetail?.resultCheck === 'ESTP' && <ESTP />}
                      {dataQuestionDetail?.resultCheck === 'INFJ' && <INFJ />}
                      {dataQuestionDetail?.resultCheck === 'INFP' && <INFP />}
                      {dataQuestionDetail?.resultCheck === 'INTP' && <INTP />}
                      {dataQuestionDetail?.resultCheck === 'ISFJ' && <ISFJ />}
                      {dataQuestionDetail?.resultCheck === 'ISFP' && <ISFP />}
                      {dataQuestionDetail?.resultCheck === 'ISTJ' && <ISTJ />}
                      {dataQuestionDetail?.resultCheck === 'ISTP' && <ISTP />}
                    </Grid>
                    <Grid sx={{ display: 'flex', justifyContent: 'right', alignItems: 'center', marginTop: '20px' }}>
                      <Button onClick={handleBack} size='large' variant="outlined" sx={{ marginRight: '10px', width: '150px', height: '50px', gap: '10px' }}>Quay về</Button>
                    </Grid>
                  </MainCardV2>
                  <MainCardV2 title={`Danh sách câu hỏi đã trả lời - ${dataQuestionDetail?.name}`} sx={{ marginTop: '20px' }}>
                    <Grid
                      container
                      spacing={gridSpacing}
                      sx={{ flexWrap: "nowrap", justifyContent: "space-between", }}
                    >
                    </Grid>
                    <TableContainer sx={{ maxHeight: 440 }}>
                      <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                          <TableRow>
                            <TableCell >SỐ THỨ TỰ</TableCell>
                            <TableCell align="left">CÂU HỎI</TableCell>
                            <TableCell align="left">ĐÁP ÁN A</TableCell>
                            <TableCell align="left">ĐÁP ÁN B</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {DataQuestion
                            .map((row: KeyedObject, index: number) => (
                              <TableRow
                                sx={{ py: 3 }}
                                hover
                                role="checkbox"
                                tabIndex={-1}
                                key={row.code}
                              >
                                <TableCell component="th" scope="row">
                                  {index + 1}
                                </TableCell>
                                <TableCell align="left">{row.question}</TableCell>
                                <TableCell align="left">
                                  {dataQuestionDetail?.arrayAnswer.length !== 0 &&
                                    <FormControlLabel control={<Checkbox checked={dataQuestionDetail?.arrayAnswer[index] === row.answer.A} disabled={dataQuestionDetail?.arrayAnswer[index] === row.answer.B} value={row} />} label={row.answer.A} />
                                  }
                                </TableCell>
                                <TableCell align="left">
                                  {dataQuestionDetail?.arrayAnswer.length !== 0 &&
                                    <FormControlLabel control={<Checkbox checked={dataQuestionDetail?.arrayAnswer[index] === row.answer.B} disabled={dataQuestionDetail?.arrayAnswer[index] === row.answer.A} value={row} />} label={row.answer.B} />
                                  }
                                </TableCell>
                              </TableRow>
                            ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                    <Grid sx={{ display: 'flex', justifyContent: 'right', alignItems: 'center', marginTop: '20px', gap: '20px' }}>
                      <Button onClick={handleBack} size='large' variant="outlined" sx={{ marginRight: '10px', width: '150px', height: '50px', gap: '10px' }}>Quay về</Button>
                    </Grid>
                  </MainCardV2>
                </>
                :
                <CircularProgress />
            }
          </>
        }
      </>
    </>
  );
}
