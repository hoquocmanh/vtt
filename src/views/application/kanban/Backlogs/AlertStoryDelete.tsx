// material-ui
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';

// types
interface Props {
    title: string;
    open: boolean;
    handleClose: (status: boolean) => void;
}

// ==============================|| KANBAN BACKLOGS - STORY DELETE ||============================== //

export default function AlertStoryDelete({ title, open, handleClose }: Props) {
    return (
        <Dialog
            open={open}
            onClose={() => handleClose(false)}
            keepMounted
            maxWidth="xs"
            aria-labelledby="item-delete-title"
            aria-describedby="item-delete-description"
        >
            {open && (
                <>
                    <DialogTitle sx={{color: '#000', display: 'flex' , justifyContent: 'center', alignItems:'center'}} id="item-delete-title">Thông báo xác nhận</DialogTitle>
                    <DialogContent sx={{display: 'flex' , justifyContent: 'center', alignItems:'center', color:'#680102'}}>
                        <DialogContentText color='#680102' sx={{fontWeight: 900, fontSize:'16px'}} id="item-delete-description">
                            Chắc chắn rằng bạn muốn hoàn thành!
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions sx={{ mr: 2 }}>
                        <Button onClick={() => handleClose(false)} color="error">
                            Hủy bỏ
                        </Button>
                        <Button size="small" onClick={() => handleClose(true)} autoFocus>
                            Xác nhận
                        </Button>
                    </DialogActions>
                </>
            )}
        </Dialog>
    );
}
