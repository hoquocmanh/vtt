import React, { Ref } from 'react';

// material-ui
import { Card, CardContent, CardContentProps, CardHeader, CardHeaderProps, CardProps, Divider, Grid, Typography } from '@mui/material';
import { useTheme } from '@mui/material/styles';

// project imports
import { KeyedObject } from 'types';

// constant
const headerSX = {
    '& .MuiCardHeader-action': { mr: 0 },
    '& .MuiCardHeader-title': {color: '#000'}
};
const headerSXContent = {
    '& .MuiCardHeader-action': { mr:0 },
    '& .MuiCardHeader-title': {fontSize: '1rem', marginTop: '-2rem', color: '#000'}
};
const headerSXContentv1 = {
    '& .MuiCardHeader-action': { mr:0 },
    '& .MuiCardHeader-title': {fontSize: '1rem', marginTop: '-3rem', color: '#000'}
};
const headerSXContentv2 = {
    '& .MuiCardHeader-action': { mr:0 },
    '& .MuiCardHeader-title': {fontSize: '1rem', marginTop: '-4rem', color: '#000'}
};

// ==============================|| CUSTOM MAIN CARD ||============================== //

export interface MainCardProps extends KeyedObject {
    border?: boolean;
    boxShadow?: boolean;
    children: React.ReactNode | string;
    style?: React.CSSProperties;
    content?: boolean;
    className?: string;
    contentClass?: string;
    contentSX?: CardContentProps['sx'];
    darkTitle?: boolean;
    sx?: CardProps['sx'];
    secondary?: CardHeaderProps['action'];
    shadow?: string;
    elevation?: number;
    title?: React.ReactNode | string;
    titleContent?: React.ReactNode | string;
    titleContent1?: React.ReactNode | string;
    titleContent2?: React.ReactNode | string;
}

const MainCardV2 = React.forwardRef(
    (
        {
            border = true,
            boxShadow,
            children,
            content = true,
            contentClass = '',
            contentSX = {},
            darkTitle,
            secondary,
            shadow,
            sx = {},
            title,
            titleContent,
            titleContent1,
            titleContent2,
            handleOpen,
            ...others
        }: MainCardProps,
        ref: Ref<HTMLDivElement>
    ) => {
        const theme = useTheme();

        return (
            <Card
                ref={ref}
                {...others}
                sx={{
                    border: border ? '1px solid' : 'none',
                    borderColor: theme.palette.mode === 'dark' ? theme.palette.background.default : theme.palette.primary[200] + 75,
                    ':hover': {
                        boxShadow: boxShadow
                            ? shadow ||
                              (theme.palette.mode === 'dark' ? '0 2px 14px 0 rgb(33 150 243 / 10%)' : '0 2px 14px 0 rgb(32 40 45 / 8%)')
                            : 'inherit'
                    },
                    ...sx
                }}
            >
                {/* card header and action */}
                <Grid sx={{display: 'flex', alignItems: 'left', flexDirection: 'column', gap:'0px'}}>
                    {!darkTitle && title && <CardHeader sx={headerSX} title={title} action={secondary} />}
                    {!darkTitle && titleContent && <CardHeader sx={headerSXContent} title={titleContent} action={secondary} />}
                    {!darkTitle && titleContent1 && <CardHeader sx={headerSXContentv1} title={titleContent1} action={secondary} />}
                    {!darkTitle && titleContent2 && <CardHeader sx={headerSXContentv2} title={titleContent2} action={secondary} />}
                </Grid>
                {darkTitle && title && (
                    <CardHeader sx={headerSX} title={<Typography variant="h3">{title}</Typography>} action={secondary} />
                )}

                {/* content & header divider */}
                {title && <Divider />}

                {/* card content */}
                {content && (
                    <CardContent sx={contentSX} className={contentClass}>
                        {children}
                    </CardContent>
                )}
                {!content && children}
            </Card>
        );
    }
);

export default MainCardV2;
